#!/bin/sh

workspace=$PWD
declare -a typo3roots
typo3roots[0]="../45.typo3.local"
typo3roots[1]="../47.typo3.local"
typo3roots[2]="../60.typo3.local"

link() {
	cd $typo3root/typo3conf/ext
	ln -s $workspace/$1 $1
	echo "Created Link to $1 in $PWD"
	cd $workspace
}

case $2 in
	45)
		ECHO "TYPO3 4.5"
		typo3root=${typo3roots[0]}
		link $1
		;;
	47)
		ECHO "TYPO3 4.7"
		typo3root=${typo3roots[1]}
		link $1
		;;
	60)
		ECHO "TYPO3 6.0"
		typo3root=${typo3roots[2]}
		link $1
		;;
	*)
		ECHO "TYPO3 4.5 - 6.0"
		for typo3root in ${typo3roots[@]}
		do
			link $1
		done
		;;
esac