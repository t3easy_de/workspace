#!/bin/sh

projectroot=$PWD
declare -a typo3roots
typo3roots[0]="../../45.typo3.local"
typo3roots[1]="../../47.typo3.local"
typo3roots[2]="../../60.typo3.local"

test() {
	echo $PWD
	cd $typo3root
	base=$(basename $(pwd))
	ECHO "Starting unit tests in $base"
	php typo3/cli_dispatch.phpsh phpunit $1
	cd $projectroot
}

case $2 in
	45)
		ECHO "TYPO3 4.5"
		typo3root=${typo3roots[0]}
		test $1
		;;
	47)
		ECHO "TYPO3 4.7"
		typo3root=${typo3roots[1]}
		test $1
		;;
	60)
		ECHO "TYPO3 6.0"
		typo3root=${typo3roots[2]}
		test $1
		;;
	*)
		ECHO "TYPO3 4.5 - 6.0"
		for typo3root in ${typo3roots[@]}
		do
			test $1
		done
		;;
esac
